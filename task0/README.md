
Dima@DESKTOP-JR292H3 MINGW64 ~
$ git config --global user.name "Dima Yanickiy"                 - создаём глобальный конфиг

Dima@DESKTOP-JR292H3 MINGW64 ~
$ git config --global user.email "yanickiydima2003@gmail.com"   - создаём глобальный конфиг


$ git clone https://gitlab.com/dimas_yanic/pytonprojects.git    - сохраняем репо
git commit -m "add README"                                      - делаем первый коммит и добавляем файл ридми    
git push -u origin mainCloning into 'pytonprojects'...          - пушим репу
warning: You appear to have cloned an empty repository.

Dima@DESKTOP-JR292H3 MINGW64 ~
$ cd pytonprojects                                              - передвигаемся в корневую папку

Dima@DESKTOP-JR292H3 MINGW64 ~/pytonprojects (master)
$ git switch -c main                                            - меняем название ветки с master на main
Switched to a new branch 'main'

Dima@DESKTOP-JR292H3 MINGW64 ~/pytonprojects (main)
$ touch README.md                                               - обновляет дату последнего изменения файла, либо создает                                                           новый пустой файл.

Dima@DESKTOP-JR292H3 MINGW64 ~/pytonprojects (main)
$ git add README.md                                             - добавляем файл который хотим запушить(если файлов много можно вместо прописания каждого файла поставить . "git add .")

Dima@DESKTOP-JR292H3 MINGW64 ~/pytonprojects (main)
$ git commit -m "add README"                                    - коммитим
[main (root-commit) 55d40cc] add README
 1 file changed, 0 insertions(+), 0 deletions(-)
 create mode 100644 README.md

 Dima@DESKTOP-JR292H3 MINGW64 ~/homework_dima_yanickiy/pytonprojects (main)
$ git add .                                                     - добавляем такс 0

Dima@DESKTOP-JR292H3 MINGW64 ~/homework_dima_yanickiy/pytonprojects (main)
$ git status                                                    - проверяем изменения
On branch main
Your branch is up to date with 'origin/main'.

Changes to be committed:
  (use "git restore --staged <file>..." to unstage)
        new file:   lab1.py


Dima@DESKTOP-JR292H3 MINGW64 ~/homework_dima_yanickiy/pytonprojects (main)
$ git commit -m "First commit - task 0"                         - коммитим
[main f43cde4] First commit - task 0
 1 file changed, 13 insertions(+)
 create mode 100644 lab1.py


Dima@DESKTOP-JR292H3 MINGW64 ~/homework_dima_yanickiy/pytonprojects (main)
$ git push origin main                                          - пушим первое задание
Enumerating objects: 6, done.
Counting objects: 100% (6/6), done.
Delta compression using up to 4 threads
Compressing objects: 100% (3/3), done.
Writing objects: 100% (5/5), 895 bytes | 895.00 KiB/s, done.
Total 5 (delta 0), reused 0 (delta 0), pack-reused 0
To https://gitlab.com/dimas_yanic/pytonprojects.git
   f43cde4..d674a0d  main -> main


