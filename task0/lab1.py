import random
print("A list of 30 random integers from -100 to +100: ")
array = range(-100, 100)
num = random.sample(array, 30)
print(num)
print("Maximum list item: ")
print(max(num))
print("Sequence number of the maximum list item:")
print(num.index(max(num)))
print("Pairs of negative numbers standing side by side:\n")
for i in range(len(num)-1):
    if num[i] < 0 and num[i+1] < 0:
        print(num[i], num[i+1])